package tema1;

import net.datastructures.ArrayQueue;
import net.datastructures.ArrayStack;
import net.datastructures.Queue;
import net.datastructures.Stack;

public class exercitiul2 {

	public static void main(String[] args) {
//		2. Se da urmatorul array:
//		{"A","B","C","D","E","F","G","H"}
//
//		-Preluati primele 5 elemente intr-o stiva/coada;
//		-Transferati din stiva/coada primele 2 elemete intr-o coada/stiva;
//		-Preluati ultimele 3 elemente in coada/stiva;
//		-Implementati o metoda intersect si minus intre stiva si coada/coada si stiva;
//		-Verificati daca elementul A/H se mai afla in stiva/coada.
		
		String[] Vector = {"A","B","C","D","E","F","G","H"}; 
		
		//1 implementare stive si cozi
		Stack<String> Stiva = new ArrayStack<String>();
		for (int i = 0; i < 5; i++)
			Stiva.push(Vector[i]);

		Queue<String> Coada = new ArrayQueue<String>();
		for (int i = 0; i < 5; i++)
			Coada.enqueue(Vector[i]);
		System.out.println("Stiva: " + Stiva);
		System.out.println("Queue: " + Coada);
		System.out.println("__________________________");
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//2 transfer din coada in stiva si din stiva in coada
		Coada.enqueue(Stiva.pop());
		Coada.enqueue(Stiva.pop());
		
		Stiva.push(Coada.dequeue());
		Stiva.push(Coada.dequeue());
		
		System.out.println("Stiva: " + Stiva);
		System.out.println("Queue: " + Coada);
		System.out.println("__________________________");
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//3 scriem ultimele elemente in stiva si coada
		for (int i = Vector.length-1;i>Vector.length-1-3; i--)
			Coada.enqueue(Vector[i]);
		for (int i = Vector.length-1;i>Vector.length-1-3; i--)
			Stiva.push(Vector[i]);
		
		System.out.println("Stiva: " + Stiva);
		System.out.println("Queue: " + Coada);
		System.out.println("__________________________");
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//4.1
		//Am salvat valorile din stiva si coada, in 2 vectori
		String[] VectorStiva = new String[Stiva.size()+1];
		String[] VectorCoada = new String[Coada.size()+1];
				
		for(int i=Stiva.size()-1;i>-1;i--) 
			VectorStiva[i] = Stiva.pop();
		
		for(int i=0;i<=VectorCoada.length-1;i++)
			VectorCoada[i] = Coada.dequeue();
		
		//Repopulam stiva si coada	
		for(int i=0;i<VectorStiva.length-1;i++) {
			Stiva.push(VectorStiva[i]);
			Coada.enqueue(VectorCoada[i]);
		}
			
		System.out.println("Stiva: " + Stiva);
		System.out.println("Queue: " + Coada);
				

		//4.2
		//declaram 2 vectori, intersect si minus unde salvam valorile intersectiei si minusul dintre stiva si coada
		String[] intersect = new String[8];
		String[] minus = new String[8];
		boolean ok;
			
		//folosim cei 2 vectori pentru a determina intersectia si minusul
		//aceasta este functia propriuzisa de intersect si minus
		for(int i=0;i<VectorStiva.length-1;i++) {
			ok=false;	
			for(int j=0;j<VectorCoada.length-1;j++) 
				if(VectorStiva[i]==VectorCoada[j]) {
					intersect[i]=VectorStiva[i];
					ok=true;
					break;
				}
			if(ok==false)
				minus[i]=VectorStiva[i];
		}

		//4.3 afisarea 
		//afisam tot ce aveme :))
		System.out.print("VectorStiva: ");
		for(int i=0;i<VectorStiva.length-1;i++)
			System.out.print(VectorStiva[i]+" ");
		
		System.out.println("");
		System.out.print("VectorCoada: ");
		for(int i=0;i<VectorCoada.length-1;i++)
			System.out.print(VectorCoada[i]+" ");
		
		System.out.println("");
		System.out.print("Intersectia: ");
		for(int i=0;i<8;i++)
			if(intersect[i]!=null)
				System.out.print(intersect[i]+" ");
		
		System.out.println("");
		System.out.print("Minus: ");
		for(int i=0;i<8;i++)
			if(minus[i]!=null)
				System.out.print(minus[i]+" ");
		System.out.println("__________________________");
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//5
		//verificam daca stiva contine A sau H;
		System.out.println(Stiva);
		boolean okA = false;
		boolean okH = false;
		int n=Stiva.size();
		for(int i=0;i<n;i++) {
			String obiect = Stiva.pop();
			if(obiect=="A")
				okA=true;
			if(obiect=="H")
				okH=true;
		}
		if(okA==true)
			System.out.println("Stiva detine elementul A");
		else
			System.out.println("Stiva nu detine elementul A");
		if(okH==true)
			System.out.println("Stiva detine elementul H");
		else
			System.out.println("Stiva nu detine elementul H");
		
		//verificam daca coada contine A sau H;
		System.out.println("________________________");
		System.out.println(Coada);
		okA=false;
		okH=false;
		n=Coada.size();
		for(int i=0;i<n;i++) {
			String obiect = Coada.dequeue();
			if(obiect=="A")
				okA=true;
			if(obiect=="H")
				okH=true;
		}
		
		if(okA==true)
			System.out.println("Coada detine elementul A");
		else
			System.out.println("Coada nu detine elementul A");
		if(okH==true)
			System.out.println("Coada detine elementul H");
		else
			System.out.println("Coada nu detine elementul H");
	}

}
