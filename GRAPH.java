package tema1;

import net.datastructures.AdjacencyMapGraph;
import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.LinkedPositionalList;
import net.datastructures.Map;
import net.datastructures.SortedTableMap;
import net.datastructures.UnsortedTableMap;
import net.datastructures.Vertex;

public class exercitiul6 {

	public static void main(String[] args) {
//		6. Se da urmatorul vector reprezentand localitati:
//		{"IS","BT","VS","PN","SV", "BC"}
//		si un tablou bidimensional care descrie legaturile dintre localitati:
//		{
//			{"IS","BT",120},
//			{"IS","VS",70},
//			{"PN","SV",80},
//			{"IS","SV",110},
//			{"IS","PN",90},
//			{"PN","BC",100},
//			{"BC","SV",140},
//			{"VS","BC",200},
//		}
//
//		- Instantiati si populati un graph direcționat (varfurile reprezinta localitatile, muchiile fiind
//		date de legaturile dintre acestea - tabloul bidimensional).
//		- Cautati varfurile cu etichetele "PN" si "CJ" si afisati valorile gasite;
//		- Utilizand algoritmul breadth-first/depth-first gasiti toate localitatile in care se poate ajunge
//		din "IS" prin rute mai scurte de 115 (km).
		
		String[] localitati = 	{"IS","BT","VS","PN","SV", "BC"};
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//1
		//Definim graful orientat
		Graph<String,Integer> Graf = new AdjacencyMapGraph<>(true); //graful este orientata daca e true
		
		//Adaugam nodurile
		Map<String,Vertex<String>> noduri = new SortedTableMap<>();
		for(int i=0;i<localitati.length;i++)
			noduri.put(localitati[i], Graf.insertVertex(localitati[i]));
		
		//Adaugam arcele
		Graf.insertEdge(noduri.get("IS"),noduri.get("BT") , 120);
		Graf.insertEdge(noduri.get("IS"),noduri.get("VS") , 70);
		Graf.insertEdge(noduri.get("PN"),noduri.get("SV") , 80);
		Graf.insertEdge(noduri.get("IS"),noduri.get("SV") , 110);
		Graf.insertEdge(noduri.get("IS"),noduri.get("PN") , 90);
		Graf.insertEdge(noduri.get("PN"),noduri.get("BC") , 100);
		Graf.insertEdge(noduri.get("BC"),noduri.get("SV") , 140);
		Graf.insertEdge(noduri.get("VS"),noduri.get("BC") , 200);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//2
		//cautam in nodurile dintr-un graf folosind vertices
		for(Vertex<String> E:Graf.vertices())
			if(E.getElement()=="PN"||E.getElement()=="CJ")
				System.out.println(E.getElement());
		System.out.println("________________________");
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//3
		//DFS

		Map<Vertex<String>,Integer> vizDFS = new UnsortedTableMap<>();
		Map<Vertex<String>,Edge<Integer>> anteriorDFS = new UnsortedTableMap<>();
		
		DFS(Graf,getVertex(Graf, "IS"),vizDFS,anteriorDFS,0);
		
		for(Vertex<String> nod: vizDFS.keySet())
			System.out.println(nod.getElement()+" "+vizDFS.get(nod));
		System.out.println("________________________");
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//BFS
		Map<Vertex<String>,Integer> vizBFS = new UnsortedTableMap<>();
		Map<Vertex<String>,Edge<Integer>> anteriorBFS = new UnsortedTableMap<>();
		
		BFS(Graf, getVertex(Graf,"IS"),vizBFS,anteriorBFS);
		
		for(Vertex<String> nod: vizBFS.keySet())
			System.out.println(nod.getElement()+" "+vizBFS.get(nod));
		System.out.println("________________________");
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//3 - FUNCTIE DFS
	public static void DFS(Graph<String, Integer> g, Vertex<String> src,
			Map<Vertex<String>, Integer> known, Map<Vertex<String>, Edge<Integer>> forest,int val) {
		known.put(src, val);
		for(Edge<Integer> e : g.outgoingEdges(src)) {
			Vertex<String> varfAdiacent = g.opposite(src,  e);
			if(known.get(varfAdiacent) == null) {
				forest.put(varfAdiacent, e);
				DFS(g, varfAdiacent, known, forest,val+e.getElement());
			}
			else {
				known.put(varfAdiacent, known.get(varfAdiacent) + 1);
			}
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//3 - FUNCTIE nod
	public static Vertex<String> getVertex(Graph<String, Integer> g, String etichetaVarfCautat) {
		for (Vertex<String> varf : g.vertices()) {
			if(varf.getElement().equals(etichetaVarfCautat)) {
				return varf;
			}
		}
		return null;
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//3 - FUNCTIE BFS
	public static void BFS(Graph<String, Integer> g, Vertex<String> src,
			Map<Vertex<String>, Integer> known, Map<Vertex<String>, Edge<Integer>> forest) {
		known.put(src,  0);
		LinkedPositionalList<Vertex<String>> level = new LinkedPositionalList<>();
		level.addLast(src);
		while(!level.isEmpty()) {
			LinkedPositionalList<Vertex<String>> nextLevel = new LinkedPositionalList<>();
			for(Vertex<String> v : level) 
			{
				for(Edge<Integer> e : g.outgoingEdges(v))
				{
					Vertex<String> varfAdiacent = g.opposite(v, e);
					if(known.get(varfAdiacent) == null) 
					{
						known.put(varfAdiacent, 1);
						forest.put(varfAdiacent, e);
						nextLevel.addLast(varfAdiacent);
					}
					else
					{
						known.put(varfAdiacent, known.get(varfAdiacent) + 1);
					}
				}
			}
			level = nextLevel;
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
	

