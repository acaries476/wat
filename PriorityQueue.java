package tema1;

import java.util.Comparator;

import net.datastructures.Entry;
import net.datastructures.PriorityQueue;
import net.datastructures.SinglyLinkedList;
import net.datastructures.UnsortedPriorityQueue;

public class exercitiul4 {

	public static void main(String[] args) {
//		4. Se dau urmatorii vectori:
//
//			persoane {"Ana","Ion","Andrei","Vasile","Vlad","Traian","Olga"}
//			varste {34, 78, 12, 45, 31, 25, 8}
//
//			- Instantiati si populati o structura de tip PriorityQueue (prioritatile fiin date de cel 
//		de-al doilea vector) astfel incat primele persoane sunt cu varsta cea mai mica/primele persoane
//		sunt cu varsta cea mai mare
//			- Extrageti din coada primele 3 elemente si adaugati-le intr-o lista
//			- Afisati elementele din lista
		
		String[] persoane = {"Ana","Ion","Andrei","Vasile","Vlad","Traian","Olga"};
		int[] varste = {34, 78, 12, 45, 31, 25, 8};
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//1
		PriorityQueue<Integer,String> CoadaP1 = new UnsortedPriorityQueue<Integer,String>(new ComparatorAscInteger());
		PriorityQueue<Integer,String> CoadaP2 = new UnsortedPriorityQueue<Integer,String>(new ComparatorDescInteger());
		
		//Populare cozi
		for(int i=0;i<persoane.length;i++) {
			CoadaP1.insert(varste[i], persoane[i]);
			CoadaP2.insert(varste[i], persoane[i]);
		}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//2 si 3
		//extragem primele 3
		//parcurgere pentru prima coada
		Entry<Integer, String> Element;
		SinglyLinkedList<String> persoanele1 = new SinglyLinkedList<String>();
		SinglyLinkedList<Integer> varstele1 = new SinglyLinkedList<Integer>();
		int i=1;
		while(i<4) {
			Element = CoadaP1.removeMin();
			persoanele1.addLast(Element.getValue());
			varstele1.addLast(Element.getKey());
			i++;
		}
		//afisam elementele
		System.out.println(persoanele1);
		System.out.println(varstele1);
		System.out.println("________________________");

		//parcurgere pentru a doua coada
		SinglyLinkedList<String> persoanele2 = new SinglyLinkedList<String>();
		SinglyLinkedList<Integer> varstele2 = new SinglyLinkedList<Integer>();
		i=1;
		while(i<4) {
			Element = CoadaP2.removeMin();
			persoanele2.addLast(Element.getValue());
			varstele2.addLast(Element.getKey());
			i++;
		}
		//afisam elementele
		System.out.println(persoanele2);
		System.out.println(varstele2);
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static class ComparatorAscInteger implements Comparator<Integer>{

		public int compare(Integer i1, Integer i2) {
			return -i1.compareTo(i2);
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static class ComparatorDescInteger implements Comparator<Integer>{

		public int compare(Integer i1, Integer i2) {
			return i1.compareTo(i2);
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
