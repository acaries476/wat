package tema1;

import net.datastructures.LinkedBinaryTree;
import net.datastructures.Position;
import net.datastructures.SinglyLinkedList;


public class exercitiul5 {

	public static void main(String[] args) {
//		5. Se da urmatorul vector:
//		{"A","B","C","D","E","F","G"}
//
//		- Daca elementele din vector sunt considerate in ordine: preorder/postorder/inorder/breadthfirst 
//		populati un arbore binar cu inaltimea 2.
//		- Implementati o metoda prin care sa parcurgeti si sa extrageti intr-o lista elementele in ordinea 
//		breadthfirst/inorder/postorder/preorder.
//		- Afisati elementele returnate de metoda anterioara.
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//1 
		String[] vector = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
		
		LinkedBinaryTree<String> arboreBreadthFirst = new LinkedBinaryTree<String>();
		LinkedBinaryTree<String> arborePreOrder = new LinkedBinaryTree<String>();
		LinkedBinaryTree<String> arboreInOrder = new LinkedBinaryTree<String>();
		LinkedBinaryTree<String> arborePostOrder = new LinkedBinaryTree<String>();
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//construim arborele(vezi mai jos)
		constructieArbore(arboreBreadthFirst,2);
		constructieArbore(arborePreOrder,2);
		constructieArbore(arboreInOrder,2);
		constructieArbore(arborePostOrder,2);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//introducem elementele in arbore;
		//breadthfirst;
		int i=0;
		for(Position<String> E:arboreBreadthFirst.breadthfirst()) {
			arboreBreadthFirst.set(E, vector[i]);
			i++;
			if(i==vector.length)
				break;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//preorder;
		i=0;
		for(Position<String> E:arborePreOrder.preorder()) {
			arborePreOrder.set(E, vector[i]);
			i++;
			if(i==vector.length)
				break;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//inorder
		i=0;
		for(Position<String> E:arboreInOrder.inorder()) {
			arboreInOrder.set(E, vector[i]);
			i++;
			if(i==vector.length)
				break;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//postorder;
		i=0;
		for(Position<String> E:arborePostOrder.postorder()) {
			arborePostOrder.set(E, vector[i]);
			i++;
			if(i==vector.length)
				break;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//2 //parcurgere arbori
		SinglyLinkedList<String> ListaBreadthFirst = new SinglyLinkedList<>();
		SinglyLinkedList<String> ListaPreOrder = new SinglyLinkedList<>();
		SinglyLinkedList<String> ListaInOrder = new SinglyLinkedList<>();
		SinglyLinkedList<String> ListaPostOrder = new SinglyLinkedList<>();
		
		for(Position<String> E:arboreBreadthFirst.breadthfirst())
			ListaBreadthFirst.addLast(E.getElement());
		System.out.println(ListaBreadthFirst);
		
		for(Position<String> E:arborePreOrder.preorder())
			ListaPreOrder.addLast(E.getElement());
		System.out.println(ListaPreOrder);
		
		for(Position<String> E:arboreInOrder.inorder())
			ListaInOrder.addLast(E.getElement());
		System.out.println(ListaInOrder);
		
		for(Position<String> E:arborePostOrder.postorder())
			ListaPostOrder.addLast(E.getElement());
		System.out.println(ListaPostOrder);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//ignorati asta
	}
	
	public static void addPreorder(LinkedBinaryTree<String> arbore1,String[] vector,int nivele) {
		int nivelActual = 1, i=2,indexdrp = 0;
		boolean ok=true;
		arbore1.addRoot(vector[0]);
		arbore1.addLeft(arbore1.root(), vector[1]);
		Position<String> pozitie = arbore1.left(arbore1.root());
		while(i<vector.length) {
			arbore1.addLeft(pozitie, vector[i]);
			i++;
			if(nivelActual==nivele-1) {
				ok=false;
				nivelActual=nivelActual-1;
				arbore1.addRight(pozitie, vector[i]);
				i++;
				pozitie=arbore1.parent(pozitie);
				indexdrp++;
			}
			if(ok==true) {
				pozitie=arbore1.left(pozitie);
				nivelActual++;
			}
			else if(indexdrp<Math.pow(2,nivele)-1){
				arbore1.addRight(pozitie,vector[i]);
				i++;
				pozitie=arbore1.right(pozitie);
				indexdrp++;
				ok=true;
			}
			else break;
		}
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//1 functie
	//construim arborele gol;
	public static void constructieArbore(LinkedBinaryTree<String> arbore1, int nr_nivele) {
		SinglyLinkedList<Position<String>> constructie = new SinglyLinkedList<Position<String>>();
		int nivel=0;
		arbore1.addRoot(null);
		constructie.addLast(arbore1.root());
		while(arbore1.height(arbore1.root())<nr_nivele) {
			for(int j=0;j<Math.pow(2,nivel);j++) {
				Position<String> elem=constructie.removeFirst();
				arbore1.addLeft(elem, "");
				arbore1.addRight(elem, "");
				constructie.addLast(arbore1.left(elem));
				constructie.addLast(arbore1.right(elem));			
			}
			nivel++;
		}
	}
}


