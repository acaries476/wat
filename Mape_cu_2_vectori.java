package tema1;

import net.datastructures.Map;
import net.datastructures.UnsortedTableMap;

public class exercitiul3 {

	public static void main(String[] args) {
//		3. Se dau urmatorii vectori:
//
//			persoane {"Ana","Ion","Andrei","Vasile","Vlad","Traian","Olga"}
//			varste {34, 78, 12, 45, 31, 25, 8}
//
//			- Instantiati o structura de tip Map astfel incat elementele din primul vector
//			sa fie asociate cu cele din al doilea vector
//			- Populati tabela asociativa (elementele din primul tablou reprezinta cheile,
//			cele din al doilea valorile)
//			- Cautati si apoi afisati persoana cea mai tanara si persoana cea mai in varsta 
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//1 si 2 nush care e faza tho
		
			Map<String,Integer> Mapa = new UnsortedTableMap<>();
			
			String[] persoane = {"Ana","Ion","Andrei","Vasile","Vlad","Traian","Olga"};
			int[] varste = {34, 78, 12, 45, 31, 25, 8};
			
			for(int i=0;i<persoane.length;i++) {
				Mapa.put(persoane[i], varste[i]);
			}
			
			for(String key:Mapa.keySet()) {
				System.out.println(key+": "+Mapa.get(key));
			}
			System.out.println("________________________");

////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//3
			String keyMin= "";
			String keyMax="";
			Integer min=10000;
			Integer max=-1;
			for(String key:Mapa.keySet()) {
				if(Mapa.get(key)<min) {
					keyMin=key;
					min=Mapa.get(key);
				}
				if(Mapa.get(key)>max) {
					keyMax=key;
					max=Mapa.get(key);
				}
			}
			System.out.println(keyMin);
			System.out.println(keyMax);
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

}
