package tema1;

import net.datastructures.ArrayList;
import net.datastructures.LinkedPositionalList;
import net.datastructures.Position;
import net.datastructures.PositionalList;

import net.datastructures.*;

public class exercitiul1 {
//	//1. Se da urmatorul array:
//	{25,1,6,89,4,2,15,6}
//
//	- Instantiati o structura de tip PositionalList<Integer>/ArrayList<Integer>/SinglyLinkedList<Integer> 
	//si preluati elementele din array in lista.
//	- Elimitati din lista al treilea si ultimul element 
//	- Implementati o metoda care va sterge toate elementele/pozitiile din lista la care se gaseste o valoare
	//data si returneaza numarul elementelor sterse

	public static void main(String[] args) {
		 int[] vector =  {25,1,6,89,4,2,15,6};
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 
		 //1
		 PositionalList<Integer> PList = new LinkedPositionalList<Integer>();
		 ArrayList<Integer> AList = new ArrayList<Integer>();
		 SinglyLinkedList<Integer> SList = new SinglyLinkedList<Integer>();
		 
		 //inserare in liste
		 for(int i=0;i<vector.length;i++) {
			 PList.addLast(vector[i]);
			 AList.add(i, vector[i]);
			 SList.addLast(vector[i]);
		 }
		 
		 System.out.println(PList);
		 System.out.println(AList);
		 System.out.println(SList);
		 System.out.println("________________________");
////////////////////////////////////////////////////////////////////////////////////////////////////////////		 
		 
		//2
		//stergere al treilea si ultimul element
		 
		//PositionalList
		int i=1;
		for(Position<Integer> E: PList.positions()) {
			if(i==3) {
				PList.remove(E);
				break;
			}
			i++;
		}
		PList.remove(PList.last());
		
		//ArrayList
		AList.remove(2);
		AList.remove(AList.size()-1);
		
		//SinglyLinkedList
		for(i=0;i<vector.length;i++)
			SList.removeFirst();
		for(i=0;i<vector.length;i++) {
			if(i!=2&&i!=vector.length-1) {
				SList.addLast(vector[i]);
			}
		}
		
		System.out.println(PList);
		System.out.println(AList);
		System.out.println(SList);
		System.out.println("________________________");
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//3
		System.out.println(removePList(PList, 89));
		System.out.println(removeAList(AList,89));
		System.out.println(removeSList(SList,89));
		
		System.out.println(PList);
		System.out.println(AList);
		System.out.println(SList);
		System.out.println("________________________");
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	//3 -> functii
	public static int removePList(PositionalList<Integer> PList, int valoare) {
		int k=0;
		for(Position<Integer> E: PList.positions()) {
			if(E.getElement()==valoare) {
				PList.remove(E);
				k++;
			}
		}
		return k;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static int removeAList(ArrayList<Integer> AList, int valoare) {
		int k=0;
		for(int i=0;i<AList.size();i++) {
			if(AList.get(i)==valoare) {
				AList.remove(i);
				k++;
			}
		}
		return k;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static int removeSList(SinglyLinkedList<Integer> SList, int valoare) {
		int k=0;
		int[] aux = new int[SList.size()];
		int n = SList.size();
		for(int i=0;i<n;i++) {
			aux[i]=SList.first().intValue();
			SList.removeFirst();
		}
		for(int i=0;i<n;i++) {
			if(aux[i]==valoare) 
				k++;
			else
				SList.addLast(aux[i]);
		}
		return k;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
